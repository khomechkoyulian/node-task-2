const express = require('express');

const notesController = require('../controllers/notesController');
const authController = require('../controllers/authController');

// eslint-disable-next-line new-cap
const router = express.Router();

router
    .route('/')
    .get(authController.protect, notesController.getNotes)
    .post(authController.protect, notesController.addNote);
router
    .route('/:id')
    .get(authController.protect, notesController.getNote)
    .put(authController.protect, notesController.updateNote)
    .patch(authController.protect, notesController.toggleCompleted)
    .delete(authController.protect, notesController.deleteNote);

module.exports = router;
