const express = require('express');

const userController = require('../controllers/usersController');

// eslint-disable-next-line new-cap
const router = express.Router();

router
    .route('/')
    .get(userController.getUser)
    .delete(userController.deleteUser)
    .patch(userController.changePassword);

module.exports = router;
