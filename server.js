require('dotenv').config();

const mongoose = require('mongoose');
const app = require('./app');

const port = process.env.PORT;

const server = app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});

const DB = process.env.DATABASE.replace(
    '<password>',
    process.env.DATABASE_PASSWORD,
);

mongoose
    .connect(DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => console.log('Connection with database established.'));

process.on('unhandledRejection', (err) => {
  console.log('Unhandled Rejection. Shutting down...');
  console.log(err.name, err.message);
  server.close(() => {
    process.exit(1);
  });
});
