const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const globalErrorHandler = require('./controllers/errorController');

const usersRouter = require('./routes/usersRoutes');
const notesRouter = require('./routes/notesRoutes');
const authRouter = require('./routes/authRoutes');

const app = express();

app.use(morgan('dev'));
app.use(express.json());
app.use(cors());

app.use(globalErrorHandler);

app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);

module.exports = app;
