const jwt = require('jsonwebtoken');

module.exports = function() {
  return jwt.decode(localStorage.getItem('token')).id;
};
