const getUserId = require('./getUserId');

module.exports = function(userIdCreatedNote, currentUserId = getUserId()) {
  return userIdCreatedNote === currentUserId;
};
