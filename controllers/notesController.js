const Note = require('../models/noteModel');

const AppError = require('../utils/AppError');

const getUserId = require('../utils/getUserId');
const isAllowed = require('../utils/isAllowed');

exports.getNotes = async (req, res, next) => {
  const {offset, limit} = req.query;

  const userId = getUserId();

  if (!userId) return next(new AppError('You are not authorized!', 400));

  const notes = await Note.find({userId})
      .skip(offset || 0)
      .limit(limit || 10)
      .select('-__v');

  res.status(200).json({
    offset,
    limit,
    count: notes.length,
    notes,
  });
};

exports.addNote = async (req, res, next) => {
  const {text} = req.body;

  if (!text) return next(new AppError('Enter the text', 400));

  const id = getUserId();

  await Note.create({
    userId: id,
    text,
  });

  res.status(200).json({
    message: 'Success',
  });
};

// By ID
exports.getNote = (req, res, next) => {
  const id = req.params.id;

  Note.findById(id, (err, note) => {
    if (!note) return next(new AppError('This note does not exist', 400));

    if (err) return next(new AppError('An error occured', 500));

    if (!isAllowed(note.userId)) {
      return next(new AppError('You are not allowed to access this note', 400));
    }

    res.status(200).json({
      message: 'Success',
      note,
    });
  });
};

exports.updateNote = (req, res, next) => {
  const id = req.params.id;
  const text = req.body.text;

  Note.findByIdAndUpdate(id, {$set: {text}}, (err, note) => {
    if (!note) return next(new AppError('This note does not exist', 400));

    if (err) return next(new AppError('An error occured', 500));
    if (!isAllowed(note.userId)) {
      return next(
          new AppError('You are not allowed to update this note.', 400),
      );
    }

    res.status(200).json({message: 'Success'});
  });
};

exports.toggleCompleted = (req, res, next) => {
  const id = req.params.id;

  Note.findById(id, (err, doc) => {
    if (err) return next(new AppError('This note does not exist', 400));

    if (!isAllowed(doc.userId)) {
      return next(new AppError('You are not allowed to edit this note.', 400));
    }

    Note.updateOne(
        {_id: id},
        {$set: {completed: !doc.completed}},
        (err, doc) => {
          if (err) return next(new AppError('An error occured.', 500));

          res.status(200).json({message: 'Success'});
        },
    );
  });
};

exports.deleteNote = (req, res, next) => {
  const id = req.params.id;

  Note.findById(id, (err, doc) => {
    if (err) return next(new AppError('An error occured.', 500));

    if (!doc) return next(new AppError('This note does not exist', 400));

    if (!isAllowed(doc.userId)) {
      return next(
          new AppError('You are not allowed to delete this note.', 400),
      );
    }

    Note.deleteOne({_id: id}, (err, doc) => {
      if (err) return next(new AppError('An error occured', 500));

      res.status(200).json({message: 'Success'});
    });
  });
};
