const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
if (typeof localStorage === 'undefined' || localStorage === null) {
  const LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

const AppError = require('../utils/AppError');
const Credentials = require('../models/credentialsModel');
const User = require('../models/userModel');

const signToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRESIN,
  });
};
/**
 *
 * @param {*} candidatePassword
 * @param {*} userPassword
 * @return {boolean}
 */
async function correctPassword(candidatePassword, userPassword) {
  return await bcrypt.compare(candidatePassword, userPassword);
}

exports.registerUser = (req, res, next) => {
  Credentials.create(
      {
        username: req.body.username,
        password: req.body.password,
      },
      (err, data) => {
        if (err) return next(new AppError(err.message, 400));

        const token = signToken(data._id);
        localStorage.setItem('token', token);

        User.create({
          username: data.username,
          userId: data._id,
        });

        res.status(200).json({message: 'Success', jwt_token: token});
      },
  );
};

exports.loginUser = async (req, res, next) => {
  const {username, password} = req.body;

  // 1) Check if id and password exit
  if (!username || !password) {
    return next(new AppError('Provide user id and password.', 400));
  }

  // 2) Check if credential exists and password is correct
  const credential = await Credentials.findOne({username}).select(
      '+password -__v',
  );

  if (!credential || !correctPassword(password, credential.password)) {
    return next(
        new AppError(
            'This credential does not exists or password is not correct.',
            400,
        ),
    );
  }

  // 3) If everything is ok, send token to client
  const token = signToken(credential._id);
  localStorage.setItem('token', token);

  res.status(200).json({
    message: 'Success',
    jwt_token: token,
  });
};

exports.protect = (req, res, next) => {
  // 1) Get token and check of it's there
  const token = localStorage.getItem('token');
  if (!token) return next(new AppError('token does not exist', 400));

  // 2) Token verification
  let payload;
  try {
    payload = jwt.verify(token, process.env.JWT_SECRET);
  } catch (err) {
    return next(new AppError('token is invalid', 400));
  }

  // 3) Check if user still exits
  if (!Credentials.findById(payload.id)) {
    return next(new AppError('this user does not exist', 400));
  }

  next();
};
