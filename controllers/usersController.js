const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const AppError = require('../utils/AppError');

const User = require('../models/userModel');
const Credentials = require('../models/credentialsModel');

const getUserId = require('../utils/getUserId');

exports.getUser = (req, res, next) => {
  const userId = getUserId();

  if (!userId) return next(new AppError('You are not authorized!', 400));

  User.findOne({userId}, (err, user) => {
    if (err) return next(new AppError('An error occured', 500));

    res.status(200).json({
      user,
    });
  });
};
exports.deleteUser = (req, res, next) => {
  const userId = getUserId();
  console.log(userId);

  if (!userId) return next(new AppError('You are not authorized', 400));

  User.findOne({userId}, (err, user) => {
    if (err) return next(new AppError('An error occured', 500));
    if (!user) return next(new AppError('This user does not exist', 400));

    if (user.userId !== userId) {
      return next(
          new AppError('You are not authorized to delete this user.', 400),
      );
    }

    Credentials.deleteOne({_id: userId}, (err) => {
      if (err) return next(new AppError('An error occured.', 500));
    });
    User.deleteOne({userId}, (err) => {
      if (err) return next(new AppError('An error occured.', 500));

      res.status(200).json({message: 'Success'});
    });
  });
};
exports.changePassword = async (req, res, next) => {
  let {oldPassword, newPassword} = req.body;

  const token = req.headers.authorization.split(' ')[1];

  if (!token) {
    return next(new AppError('Token is not specified in header.', 400));
  }

  const userId = jwt.decode(token).id;

  if (!userId) return next(new AppError('You are not authorized.', 400));

  const usersCredentials = await Credentials.findById(userId).select(
      '+password',
  );

  if (!usersCredentials) {
    return next(new AppError('This user does not exist.', 400));
  }

  if (!(await bcrypt.compare(oldPassword, usersCredentials.password))) {
    return next(new AppError('You have entered a wrong password!', 400));
  }

  newPassword = await bcrypt.hash(newPassword, 12);

  Credentials.updateOne(
      {_id: userId},
      {$set: {password: newPassword}},
      (err) => {
        if (err) return next(new AppError(err.message, 500));

        res.status(200).json({message: 'Success'});
      },
  );
};
