const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const credentialsSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Enter your username.'],
    index: {
      unique: true,
    },
  },
  password: {
    type: String,
    required: [true, 'Enter your password.'],
    select: false,
  },
});

credentialsSchema.pre('save', async function(next) {
  if (!this.isModified('password')) return next();

  this.password = await bcrypt.hash(this.password, 12);
  next();
});

const Credentials = mongoose.model('Credentials', credentialsSchema);

module.exports = Credentials;
