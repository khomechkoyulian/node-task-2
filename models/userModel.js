const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Enter your username.'],
  },
  userId: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: new Date(),
  },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
