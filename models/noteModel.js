const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, 'Enter user ID.'],
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: [true, 'Enter text.'],
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;
